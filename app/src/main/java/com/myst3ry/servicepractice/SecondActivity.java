package com.myst3ry.servicepractice;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondActivity extends AppCompatActivity {

    @BindView(R.id.output_container)
    TextView outputContainer;

    private Messenger myService;
    private Messenger messenger = new Messenger(new LocalHandler());

    private ServiceConnection serviceConnection;
    private boolean isBound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                myService = new Messenger(service);
                Message msg = Message.obtain(null, MyService.MSG_REGISTER_CLIENT);
                msg.replyTo = messenger;
                try {
                    myService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                myService = null;
            }
        };

        init();
    }

    private void init() {
        bindService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unBindService();
    }

    private void bindService() {
        bindService(MyService.newIntent(this), serviceConnection, Context.BIND_AUTO_CREATE);
        isBound = true;
    }

    private void unBindService() {
        if (isBound) {
	    final Message message = Message.obtain(null, MyService.MSG_UNREGISTER_CLIENT);
	    message.replyTo = messenger;

	    try {
	        myService.send(message);
	    } catch (RemoteException re) {
	        re.printStackTrace();
	    }
		
            unbindService(serviceConnection);
            isBound = false;
        }
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, SecondActivity.class);
    }

    private final class LocalHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            outputContainer.setText(Objects.toString(msg.obj));
        }
    }
}
