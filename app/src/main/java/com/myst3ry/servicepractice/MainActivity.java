package com.myst3ry.servicepractice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_service)
    Button btnService;
    @BindView(R.id.btn_activity)
    Button btnActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setListeners();
    }

    private void setListeners(){
        btnService.setOnClickListener(new StartServiceBtnListener());
        btnActivity.setOnClickListener(new StartActivityBtnListener());
    }

    private void startService() {
        startService(MyService.newIntent(this));
    }

    private void startActivity() {
        startActivity(SecondActivity.newIntent(this));
    }

    private static final class StartServiceBtnListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ((MainActivity)v.getContext()).startService();
        }
    }

    private static final class StartActivityBtnListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ((MainActivity)v.getContext()).startActivity();
        }
    }
}
